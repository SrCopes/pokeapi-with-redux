import pokeReducer from './pokedex/pokeSlice';

const reducers = {
    pokemon: pokeReducer,
};

export default reducers;