import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {pokeFetch} from './pokeFetch';
import { selectPoke } from "./pokeSlice";
import {PokeDisplay} from './pokedisplay';

const Pokedex = () => {
    const pokedex = useSelector(selectPoke);
    const dispatch = useDispatch();
    const [pokeToDisplay, setPokeToDisplay] = useState(0);

    useEffect(() => {
        dispatch(pokeFetch());
    }, [dispatch, pokedex]);

    const handleClick = (index) =>{
        setPokeToDisplay(index);
    };

    const pokes = pokedex.map((poke, index) =>{
        return(
            <div onClick={() => handleClick(index)} key={index}>
                <h3>{poke.name}</h3>
                <div>
                    {index === pokeToDisplay ? (<PokeDisplay pokemon={poke}/>) : ("")}
                </div>
            </div>
        );
    });

    return(
        <div>
            <h1>First 11 pokemons using redux!</h1>
            <ul>{pokes}</ul>
        </div>
    );
}

export default Pokedex;