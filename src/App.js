import logo from './logo.svg';
import './App.css';
import Pokedex from './poke-features/pokedex/pokedex';

function App() {
  return (
    <div >
      <Pokedex />
    </div>
  );
}

export default App;
