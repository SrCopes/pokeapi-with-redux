import React from 'react';

//Display for the individual component

const PokeDisplay = ({pokemon}) => {
    return(
        <div>
            <h4>Name: {pokemon.name}</h4>
            <img src={pokemon.sprites.front_default} alt={pokemon.name}/>
            <p>Type: {pokemon.type}</p>
            <p>Weigth: {pokemon.weight}</p>
        </div>
    );
};

export default PokeDisplay;