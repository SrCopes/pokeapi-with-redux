export const getFirstTen = () =>(dispatch) =>{
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=11&offset=0`).then(async (response) => {
        const data = await response.json();
        dispatch({
            type: "pokemon/setPokemon",
            payload: pokeFetch(data.results),
        });
    });      
};

const pokeFetch = (results) => 
    results.forEach(async (poke) => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${poke.name}`).then(async (response) => {
    const data = await response.json();

    return data;
});
});
