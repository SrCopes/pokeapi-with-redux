import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    value: [],
    status: 'idle',
};

export const pokeSlice = createSlice({
    name: "pokemon",
    initialState, 
    reducers:{
        setPokemon: (state, action) => {
            state.value = action.payload;
        }
    },
});

export const {setPokemon} = pokeSlice.actions;

export const selectPoke = (state) => state.pokemon.value;

export default pokeSlice.reducer;